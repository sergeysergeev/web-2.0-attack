
var webSearchAttack = {
    localIp: "localhost",

    getLocalhostIp: function () {
        window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;   //compatibility for firefox and chrome
        var pc = new RTCPeerConnection({iceServers:[]}), noop = function(){};
        pc.createDataChannel("");    //create a bogus data channel
        pc.createOffer(pc.setLocalDescription.bind(pc), noop);    // create offer and set local description
        pc.onicecandidate = function(ice){  //listen for candidate events
            if(!ice || !ice.candidate || !ice.candidate.candidate)  return;
            var myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate)[1];
            console.log(ice);
            webSearchAttack.localIp = myIP;
            document.getElementById("local-ip").innerHTML = webSearchAttack.localIp;

            console.log('my IP: ',  webSearchAttack.localIp);
            pc.onicecandidate = noop;
        };

    },
    start_time: (new Date()).getTime(),

    checkPost: function (port) {
        this.start_time = (new Date()).getTime();
        var ip = "localhost"
        document.getElementById("testdiv").innerHTML = '<img src="ftp://' + ip + ':' + port +
            '" alt="" onerror="error_handler();" />';
    },
    error_handler: function error_handler(){
        console.log("error!!! time is " + ((new Date()).getTime() - this.start_time));
    }

}
function error_handler(){
   webSearchAttack.error_handler();
}
